package hrank;

import java.util.Arrays;
import java.util.Scanner;

public class Staircase {
/*
input
6

output
     #
    ##
   ###
  ####
 #####
######
 */

	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.close();
        for (int i = 0; i < n; i++) {
        	char[] array = new char[n];
        	Arrays.fill(array, 0, n - i, ' ');
        	Arrays.fill(array, n - i - 1, n, '#');
        	System.out.println(array);
        }
	}

}
