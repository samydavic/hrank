package hrank;

import java.util.Scanner;

public class InsertionSortII {

/*
input
6
1 4 3 5 6 2

output
1 4 3 5 6 2 
1 3 4 5 6 2 
1 3 4 5 6 2 
1 3 4 5 6 2 
1 2 3 4 5 6

 */
	
	public static void insertIntoSorted(int[] a) {
		for (int i = 1; i < a.length; i++) {
			int j = i;
			int v = a[i];
			while (j >= 1 && a[j - 1] > v) {
				a[j] = a[j - 1];
				j--;
			}
			a[j] = v;
			printArray(a);			
		}
	}

	/* Tail starts here */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		int[] ar = new int[s];
		for (int i = 0; i < s; i++) {
			ar[i] = in.nextInt();
		}
		in.close();
		insertIntoSorted(ar);
	}

	private static void printArray(int[] ar) {
		for (int n : ar) {
			System.out.print(n + " ");
		}
		System.out.println("");
	}

}
