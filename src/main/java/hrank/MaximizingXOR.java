package hrank;

import java.util.Scanner;

public class MaximizingXOR {

/*
input
10
15

output
7
*/
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int start = sc.nextInt();
		int end = sc.nextInt();
		sc.close();
		
		int max = -1;
		int x1 = start;
		while(x1 <= end) {
			int x2 = start;			
			while(x2 <= end) {
				int xor = x1 ^ x2;
				if (xor > max) {
					max = xor;
				}
				x2++;
			}
			x1++;
		}
		
		System.out.println(max);

	}

}
