package hrank;

import java.util.Scanner;

public class LonelyIntegerSimple {

/*
input0
1
1
output0
1

input1
3
1 1 2
output2
2

intput2
5
0 0 1 2 1

output2
2

 */
    private static int lonelyInteger(int[] a) {
    	int val = 0;
        for (int num : a) {
            val = val ^ num; // ^ is XOR operator
        }
        return val;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        in.close();
        System.out.println(lonelyInteger(a));
    }

}
