package hrank;

import java.util.Arrays;
import java.util.Scanner;

public class LonelyInteger {

/*
input0
1
1
output0
1

input1
3
1 1 2
output2
2

intput2
5
0 0 1 2 1

output2
2

 */
    private static int lonelyInteger(int[] a) {
    	Arrays.sort(a);
    	for (int i = 0; i < a.length - 1; i = i + 2) {
    		if ((a[i] ^ a[i + 1]) != 0) {
    			return a[i];    			
    		}
    	}    	
        return a[a.length - 1];
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        in.close();
        System.out.println(lonelyInteger(a));
    }

}
