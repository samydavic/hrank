package hrank;

import java.util.Scanner;

public class StockMax {
	
/*
Sample Input
3
3
5 3 2
3
1 2 100
4
1 3 1 2

Sample Output
0
197
3
*/
	
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		 int n = sc.nextInt();
         
	        for (int t = 0; t < n; t++) {
	            int numStock = sc.nextInt();
	            int[] stocks = new int[numStock];
	            for (int i = 0; i < numStock; ++i) {
	                stocks[i] = sc.nextInt();
	            }
	             
	            int[] maxSoFar = new int[numStock];
	            int max = Integer.MIN_VALUE;
	            for (int i = numStock-1; i >= 0; --i) {
	                max = Math.max(max, stocks[i]);
	                maxSoFar[i] = max;
	            }
	             
	            long sum = 0;
	            for (int i = 0; i < numStock; i++) {
	                if (maxSoFar[i] != stocks[i]){
	                    sum += maxSoFar[i] - stocks[i];
	                }
	            }
	             
	            System.out.println(sum);
	        }
	}
}
