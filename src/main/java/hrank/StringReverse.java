package hrank;

import java.util.Scanner;

public class StringReverse {

/*
input
madam

output
Yes
*/
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		sc.close();
		
		boolean palindrome = true;
		for (int i = 0; i < s.length() / 2; i++) {
			palindrome = (s.charAt(i) == s.charAt(s.length() - i - 1));
			
			if (!palindrome) {
				break;
			}
		}
		
		if (palindrome) {
			System.out.println("Yes");
		} else {
			System.out.println("No");
		}
	}

}
