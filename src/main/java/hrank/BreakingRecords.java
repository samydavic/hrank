package hrank;

import java.util.Scanner;

public class BreakingRecords {
/*
input
10
3 4 21 36 10 28 35 5 24 42

output
4 0
*/

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] score = new int[n];
		for (int i = 0; i < n; i++) {
			score[i] = sc.nextInt();
		}
		sc.close();
		
		int maxScore = score[0];
		int minScore = score[0];
		int breakHight = 0;
		int breakLow = 0;
		for (int i = 1; i < n; i++) {
			if (score[i] > maxScore) {
				maxScore = score[i]; 
				breakHight++;	
			}
			if (score[i] < minScore) {
				minScore = score[i]; 
				breakLow++;	
			}			
		}
		System.out.printf("%d %d", breakHight, breakLow);
	}

}
