package hrank;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TimeConversion {
/**
input
07:05:45PM

output
19:05:45
*/
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.nextLine();
		sc.close();
		try {
			SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm:ssa"); 
			Date d = sdf1.parse(s);			
			SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss"); 
			System.out.printf(sdf2.format(d));
		} catch (ParseException e) {
			e.printStackTrace();
		}	
	}
}
