package hrank;

import java.util.Scanner;

public class DiagonalDiff {
	
/**
Sample Input
3
11 2 4
4 5 6
10 8 -12

Sample Output
15
*/
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] matrix = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }
        int d1 = 0;
        for (int i = 0; i < n; i++) {
            d1 += matrix[i][i];
        }
        
        int d2 = 0;
        for (int i = 0 ; i < n; i++) {
            d2 += matrix[i][n - i - 1];
        }
        
        int diff = d1 - d2;

        diff = Math.abs(diff);
        
        System.out.println(diff);

        sc.close();
	}

}
