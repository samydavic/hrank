package hrank;

import java.util.Scanner;

public class InsertionSortI {

/*
input
5
2 4 6 8 3

output
2 4 6 8 8 
2 4 6 6 8 
2 4 4 6 8 
2 3 4 6 8 

 */
	
	public static void insertIntoSorted(int[] a) {
		int n = a.length;
		int v = a[n - 1];
		for (int i = n - 2; i >= 0; i--) {
			if (v < a[i]) {
				a[i + 1] = a[i];
				printArray(a);
                if (i == 0) {
                    a[i] = v;
                    printArray(a);
                }				
			} else {
				a[i + 1] = v;
				printArray(a);
				break;
			}
		}
	}

	/* Tail starts here */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		int[] ar = new int[s];
		for (int i = 0; i < s; i++) {
			ar[i] = in.nextInt();
		}
		in.close();
		insertIntoSorted(ar);
	}

	private static void printArray(int[] ar) {
		for (int n : ar) {
			System.out.print(n + " ");
		}
		System.out.println("");
	}

}
