package hrank;

import java.util.Scanner;

public class StringCompare {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.nextLine();
		int n = sc.nextInt();
		sc.close();
		
		String greater = s.substring(0, n);
		String smaller = s.substring(0, n);
		for (int i = 0; i <= s.length() - n; i++) {
			String sub1 = s.substring(i, i + n);
			if (sub1.compareTo(greater) > 0) {
				greater = sub1;
			}
			if (sub1.compareTo(smaller) < 0) {
				smaller = sub1;
			}
		}
		System.out.println(smaller);
		System.out.println(greater);
	}

}

