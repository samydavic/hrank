package hrank;

import java.util.Scanner;

public class AppleAndOranges {
/*
intput
7 11
5 15
3 2
-2 2 1
5 -6

output
1
1

 */
	public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int s = in.nextInt();
        int t = in.nextInt();
        int a = in.nextInt();
        int b = in.nextInt();
        int m = in.nextInt();
        int n = in.nextInt();
        int[] apple = new int[m];
        for(int apple_i=0; apple_i < m; apple_i++){
            apple[apple_i] = in.nextInt();
        }
        int[] orange = new int[n];
        for(int orange_i=0; orange_i < n; orange_i++){
            orange[orange_i] = in.nextInt();
        }
        in.close();
        
        int appleCount = 0;
        for(int i = 0; i < m; i++) {
            int p = a + apple[i];
            if (p >= s && p <= t) {
                appleCount++;
            }
        }
        int orangeCount = 0;
        for(int i = 0; i < n; i ++) {
            int p = b + orange[i];
            if (p >= s && p <= t) {
                orangeCount++;
            }
        }
        System.out.println(appleCount);
        System.out.println(orangeCount);
	}

}
