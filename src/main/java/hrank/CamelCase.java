package hrank;

import java.util.Scanner;

public class CamelCase {

/*
input
saveChangesInTheEditor

output
5
 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String s = sc.nextLine();
		sc.close();
		
		int count = 1;		
		for(int i = 0; i < s.length(); i++) {
			if (Character.isUpperCase(s.charAt(i))) {
				count++;
			}
		}
		
		System.out.println(count);
	}

}
