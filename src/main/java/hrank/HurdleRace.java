package hrank;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class HurdleRace {

/*
input
5 7
2 5 4 5 2

output
0
*/
		
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int k = in.nextInt();
		Integer[] height = new Integer[n];
		for (int height_i = 0; height_i < n; height_i++) {
			height[height_i] = in.nextInt();
		}
		in.close();

		Arrays.sort(height, Collections.reverseOrder());
		
		int min = 0;
		if (height.length > 0 && height[0] > k) {
			min = height[0] - k;
		}
		
		System.out.println(min);
	}
}
