package hrank;

import java.util.Scanner;

public class MarsExploration {
/*
intput
SOSSPSSQSSOR
output
3

intput
SOSSOT
output
1

*/
	public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        in.close();
        
        String[] array = new String[s.length() / 3];
        for (int i = 0; i < s.length() / 3; i++) {
        	array[i] = s.substring(i * 3, i * 3 + 3);        	
        }
        
        int count = 0;
        String sos = "SOS";
        for (int i = 0; i < array.length; i++) {
        	for (int j = 0; j < 3; j++) {
        		if (array[i].charAt(j) != sos.charAt(j)) {
        			count++;
        		}
        	}
        }
    	System.out.println(count);
	}

}
