package hrank;

import java.util.Scanner;

public class IceCreamParlor {
/*
input
2
4
5
1 4 5 3 2
4
4
2 2 4 3

output
1 4
1 2
 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		
		for (int i = 0; i < t; i++) {
			int m = sc.nextInt();
			int n = sc.nextInt();
			int[] cost = new int[n];
			for (int j = 0; j < n; j++) {
				cost[j] = sc.nextInt(); 
			}
			
			boolean match = false;
			for (int k = 0; k < n; k++) {
				for (int l = 0; l < n; l++) {
					if (k != l && cost[k] + cost[l] == m) {
						System.out.printf("%d %d\n", k + 1, l + 1);
						match = true;
						break;
					}
				}
				if (match) {
					break;
				}
			}
		}
		
		sc.close();
	}

}
