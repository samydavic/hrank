package hrank;

import java.util.Scanner;

public class SuperReducedString {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		StringBuilder sb = new StringBuilder(sc.nextLine());
		for (int i = sb.length() - 1; i > 0; i--) {
			if (sb.charAt(i) == sb.charAt(i - 1)) {
				sb.delete(i - 1, i + 1);
				i = sb.length();
			}
		}

		if (sb.length() > 0) {
			System.out.print(sb.toString());
		} else {
			System.out.print("Empty String");
		}

		sc.close();
	}

}
