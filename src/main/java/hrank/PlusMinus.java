package hrank;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class PlusMinus {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int positives = 0;
		int negatives = 0;
		int zeros = 0;
		for (int i = 0; i < n; i++) {
			int e = sc.nextInt();
			if (e > 0) {
				positives++;
			} else if (e < 0) {
				negatives++;
			} else {
				zeros++;
			}
		}
		sc.close();
		
		double positivePercentage = (double) positives / n;
		double negativesPercentage =(double) negatives / n;
		double zerosPercentage = (double) zeros / n;

		DecimalFormat df = new DecimalFormat("#.#####");
		df.setRoundingMode(RoundingMode.FLOOR);
		df.setMinimumFractionDigits(5);
		
		System.out.println(df.format(positivePercentage));
		System.out.println(df.format(negativesPercentage));
		System.out.println(df.format(zerosPercentage));
	}

}
