package hrank;

import java.util.Arrays;
import java.util.Scanner;

public class MinimumAbsoluteDifference {
/*
input
3
3 -7 0

output
3
*/
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] array = new int[n];
		for (int i = 0; i < n; i++) {
			array[i] = in.nextInt();
		}
		in.close();
		Arrays.sort(array);
		int min = Math.abs(array[n - 1] - array[0]);
		for (int i = 0; i < n - 1; i++) {
			int diff = Math.abs(array[i] - array[i + 1]);
			if (diff < min) {
				min = diff;
			}
		}
		System.out.println(min);
	}

}
