package hrank;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class PermutingTwoArrays {
/*
input
2
3 10
2 1 3
7 8 9
4 5
1 2 2 1
3 3 3 4

output
YES
NO
 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		for (int i = 0; i < t; i++) {
			int n = in.nextInt();
			int k = in.nextInt();
			Integer[] arrA = new Integer[n];
			for (int j = 0; j < n; j++) {
				arrA[j] = in.nextInt(); 
			}
			Integer[] arrB = new Integer[n];
			for (int j = 0; j < n; j++) {
				arrB[j] = in.nextInt(); 
			}
			
			// permute
			Arrays.sort(arrA);
			Arrays.sort(arrB, Collections.reverseOrder());
			
			
			// compare condition
			boolean valid = false;
			for (int j = 0; j < n; j++) {
				if (arrA[j] + arrB[j] >= k) {
					valid = true;
				} else {
					valid = false;
					break;
				}
			}
			
			if (valid) {
				System.out.println("YES");				
			} else {
				System.out.println("NO");				
			}
			
		}
		in.close();
	}

}

