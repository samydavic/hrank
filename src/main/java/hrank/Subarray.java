package hrank;

import java.util.Arrays;
import java.util.Scanner;

public class Subarray {

/*
input
5
1 -2 4 -5 1

output
9
*/
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();		
		int[] array = new int[n];
		for (int i = 0; i < n; i++) {
			array[i]= sc.nextInt();				
		}		
		sc.close();
		
		int negatives = 0;
		for (int i = 1; i <= n; i++) {
			for (int j = 0; j <= n - i; j++) {
				int[] sub = Arrays.copyOfRange(array, j, j + i);
				int sum = Arrays.stream(sub).sum();
				if (sum < 0) {
					negatives++;
				}
			}
		}
		
		System.out.println(negatives);
	}

}
