package hrank;

import java.util.Scanner;

public class SumVsXOR {
/*
input0
5
output0
2

input1
10
output1
4

input2
1000000000000000
output2
1073741824
*/
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		long n = sc.nextLong();
		sc.close();
		
		long counter = 0;
		while (n > 0) {
			counter += ((n % 2) == 0) ? 1 : 0; 
			n /= 2;
		}
		counter = (long) Math.pow(2, counter);

		System.out.println(counter);
	}

}
