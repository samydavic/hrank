package hrank;

import java.util.Scanner;

public class FlippingBits {

/*
input
3 
2147483647 
1 
0

output
2147483648 
4294967294 
4294967295

*/
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		long mask = (long) Math.pow(2, 32) - 1l;
		int n = sc.nextInt();
		for (int i = 0; i < n; i++) {
			long value = sc.nextLong();
			System.out.println(mask ^ value);
		}
		sc.close();
	}
		
}
