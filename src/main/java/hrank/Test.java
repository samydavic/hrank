package hrank;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		String[] s = new String[n + 2];
		for (int i = 0; i < n; i++) {
			s[i] = sc.next();
		}
		sc.close();
		
		Comparator<String> c = new Comparator<String>() {
			@Override
			public int compare(String arg0, String arg1) {
				BigDecimal bd0 = new BigDecimal(arg0);
				BigDecimal bd1 = new BigDecimal(arg1);
				return bd0.compareTo(bd1);
			}			
		};
		
		Arrays.sort(s, 0, n, c);
		
		// Output
		for (int i = 0; i < n; i++) {
			System.out.println(s[i]);
		}
	}

}
