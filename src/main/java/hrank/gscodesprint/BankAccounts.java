package hrank.gscodesprint;

import java.util.Scanner;

public class BankAccounts {

/*
input
3
3 20 10 60
100 200 300
3 20 15 120
200 250 300
1 1 10 100
1000

output
upfront
fee
fee

*/
    static String feeOrUpfront(int n, int k, int x, int d, int[] p) {
    	double charges = 0;
        for (int i = 0; i < p.length; i ++) {
            charges += Math.max(k, p[i] * x * 0.01);
        }
        
        if (charges <= d) {
        	return "fee";
        } else {
        	return "upfront";
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int q = in.nextInt();
        for(int a0 = 0; a0 < q; a0++){
            int n = in.nextInt();
            int k = in.nextInt();
            int x = in.nextInt();
            int d = in.nextInt();
            int[] p = new int[n];
            for(int p_i = 0; p_i < n; p_i++){
                p[p_i] = in.nextInt();
            }
            String result = feeOrUpfront(n, k, x, d, p);
            System.out.println(result);
        }
        in.close();
    }

}
