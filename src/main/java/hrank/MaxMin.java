package hrank;

import java.util.Scanner;

public class MaxMin {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		long a = in.nextLong();
		long b = in.nextLong();
		long c = in.nextLong();
		long d = in.nextLong();
		long e = in.nextLong();
		in.close();

		long[] sums = new long[5];

		sums[0] = b + c + d + e;
		sums[1] = a + c + d + e;
		sums[2] = a + b + d + e;
		sums[3] = a + b + c + e;
		sums[4] = a + b + c + d;

		long min = sums[0];
		long max = 0;
		for (int i = 0; i < 5; i++) {
			if (sums[i] > max) {
				max = sums[i];
			}
			if (sums[i] < min) {
				min = sums[i];
			}
		}
		System.out.printf("%d %d", min, max);
	}

}
