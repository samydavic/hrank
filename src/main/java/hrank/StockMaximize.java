package hrank;

import java.util.Scanner;

public class StockMaximize {
	
/*
input
3
3
5 3 2
3
1 2 100
4
1 3 1 2

output
0
197
3
 */
	private static long computeMax(int[] prices) {
		long profit = 0;
		int max = prices[prices.length - 1];
		for (int i = prices.length - 1; i > 0; i--) {
			if (prices[i - 1] < max) {
				profit += (max - prices[i - 1]);
			} else {
				max = prices[i - 1];
			}
		}
				
		return profit;		
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		for (int a0 = 0; a0 < t; a0++) {
			int N = in.nextInt();
			int[] prices = new int[N];
			for (int prices_i = 0; prices_i < N; prices_i++) {
				prices[prices_i] = in.nextInt();
			}
			System.out.println(computeMax(prices));
		}
		in.close();
	}

}
