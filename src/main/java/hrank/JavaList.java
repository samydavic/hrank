package hrank;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class JavaList {

/*
input
5
12 0 1 78 12
2
Insert
5 23
Delete
0

output
0 1 78 12 23
 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		
		List<Integer> list = new ArrayList<Integer>();
		
		for (int i = 0; i < n; i++) {
			int val = sc.nextInt();
			list.add(val);						
		}
		
		int q = sc.nextInt();
		for (int i = 0; i < q; i++) {
			sc.nextLine();
			String op = sc.nextLine();
			if (op.equals("Delete")) {
				int index = sc.nextInt();
				list.remove(index);
			} else if (op.equals("Insert")) {
				int index = sc.nextInt();
				int val = sc.nextInt();
				list.add(index, val);
			}
		}
		
		for (Integer value : list) {
			System.out.printf("%d ", value);
		}
		
		sc.close();
	}

}
