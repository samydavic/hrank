package hrank;

import java.util.Scanner;

public class GradingStudents {
/*
Sample Input 0
4
73
67
38
33

Sample Output 0
75
67
40
33
*/
	
	public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for(int a0 = 0; a0 < n; a0++){
            int grade = in.nextInt();
            int diff =  ((int) (grade / 5.0) + 1) * 5 - grade;
            int rounded = (diff <= 2) ? grade + diff : grade;
            if (rounded >= 40) {
                grade = rounded;
            }
            System.out.println(grade);
        }
        in.close();
    }
}
