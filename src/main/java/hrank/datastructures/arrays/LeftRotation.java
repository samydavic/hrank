package hrank.datastructures.arrays;

import java.util.Scanner;

public class LeftRotation {

/*
input
5 4
1 2 3 4 5

output
5 1 2 3 4
*/
    
	static int[] leftRotation(int[] a, int d) {
		int[] b = new int[a.length];
		for (int i = a.length - 1; i >= 0; i--) {
			int k = i - d >= 0 ? i - d : a.length + (i - d);
			b[k] = a[i];
		}
        return b;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int d = in.nextInt();
        int[] a = new int[n];
        for(int a_i = 0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        int[] result = leftRotation(a, d);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + (i != result.length - 1 ? " " : ""));
        }
        System.out.println("");


        in.close();
    }
}
