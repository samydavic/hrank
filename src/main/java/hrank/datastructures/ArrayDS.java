package hrank.datastructures;

import java.util.Scanner;

public class ArrayDS {

/*
input
4
1 4 3 2

output
2 3 4 1
*/

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
        int a[] = new int[n];
		for (int i = 0; i < a.length; i++) {
			a[i] = in.nextInt(); 
		}
		for (int i = a.length - 1; i >= 0; i--) {
			System.out.print(a[i] + " ");
		}
		in.close();
	}

}
